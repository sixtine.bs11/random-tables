<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>Random Tables</title>
</head>
<body>
   <h1>A TABLE ! </h1> 
<?php
$promo2 = ['Syriane','Fatima','Flo','Hayette','Nathalie','Grace', 'Isa','Maude','Robert','Esma','Esther', 'Viviane', 'Otillia','Léa','Laurie','Sixt'];

function randomPromo2($promo2) {
    $nbPersoTable = 15;
for ($y=1; $y < 5 ; $y++) { 
   
 echo "<div class=\"container\">";
    //afficher 4 prénoms sur une table
    for ($i=0; $i < 4 ; $i++) { 

        // tire aléatoirement selon le nb de personne 
        $randPromo2 = rand(0, $nbPersoTable);
        echo "<div class=\"prenoms\">";
        echo $promo2[$randPromo2];
        // détruire les prenoms sorties
        unset($promo2[$randPromo2]);
        // actualliser le tableau pour éviter les trous 
        $promo2 = array_values($promo2);
        // décrémenter la nbde personnes au fur et à mesure
        $nbPersoTable--;
        echo '&nbsp';
        echo "</div>";
        }
        echo "<div class=\"table\"><h2>Table $y</h2></div>";
        echo "</div>";
    }

}

randomPromo2($promo2);
?>
</body>
</html>



